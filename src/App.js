import React, { Component } from "react";
import "./css/tailwind.css";
import Header from "./components/Header.js";
import Footer from "./components/Footer.js";
import moment from "moment-timezone";
import _ from "lodash";
import locations from "./data/locations.json";

class App extends Component {
  constructor(props) {
    super(props);

    // Don't save the moment object in state.  Just save the unix
    // version of the time you want, and then create a new moment
    // object from that unix time whenever you need one.
    const time = moment().format("X");

    this.state = {
      locations: locations,
      time: time,
      timer: false,
      timerId: null
    };
  }

  handleClick = clickEvent => {
    const timer = this.state.timer;
    if (timer && clickEvent.target.name !== "clock") {
      return;
    }
    let time = moment.unix(this.state.time);
    switch (clickEvent.target.name) {
      case "home":
        time = moment();
        break;
      case "clock":
        time = moment();
        const timer = !this.state.timer;
        this.setState({ timer: timer });
        if (timer) {
          this.startTimer();
        } else {
          clearInterval(this.state.timerId);
        }
        break;
      case "dayUp":
        time.add(1, "days");
        break;
      case "dayDown":
        time.subtract(1, "days");
        break;
      case "hourUp":
        time.add(1, "hours");
        break;
      case "hourDown":
        time.subtract(1, "hours");
        break;
      case "minuteUp":
        time.add(1, "minutes");
        break;
      case "minuteDown":
        time.subtract(1, "minutes");
        break;
      case "ampmUp":
        time.add(12, "hours");
        break;
      case "ampmDown":
        time.subtract(12, "hours");
        break;
      default:
        break;
    }
    this.setState({
      time: time.format("X")
    });
  };

  startTimer = () => {
    console.log("start timer");
    const timerId = setInterval(() => {
      const time = moment();
      this.setState({
        time: time.format("X"),
        timerId: timerId
      });
    }, 1000);
  };

  render() {
    let time = moment.unix(this.state.time);
    const iconColor = this.state.timer ? "text-grey-lightest" : "text-grey";

    return (
      <div className="">
        <Header title="World Time Clock" time={time} timer={this.state.timer} />

        <MainPanel
          time={time}
          locations={this.state.locations}
          timer={this.state.timer}
          iconColor={iconColor}
          onClick={this.handleClick}
        />

        <Footer />
      </div>
    );
  }
}

const MainPanel = ({ time, timer, iconColor, onClick }) => {
  return (
    <div>
      <div className="flex justify-center w-full bg-grey">
        <div className="w-5/6 ">
          <BaseTimePanel
            time={time}
            timer={timer}
            iconColor={iconColor}
            onClick={onClick}
          />
        </div>
      </div>

      <div className="flex justify-center w-full">
        <div className=" w-5/6">
          <WorldTimePanel locations={locations} time={time} />
        </div>
      </div>
    </div>
  );
};

const BaseTimePanel = ({ time, timer, onClick }) => {
  const day = time.format("ddd MMM D");
  const hour = time.format("h");
  const minute = timer ? time.format("mm:ss") : time.format("mm");
  const ampm = time.format("A");
  const linkColor = timer ? "text-grey-light" : "text-white";
  const timerLabel = timer ? "Turn Clock Off" : "Turn Clock On";
  const buttonCursor = timer ? "cursor-default" : "cursor-pointer";

  return (
    <div className="bg-grey">
      <div className="flex justify-center flex-col lg:flex-row w-full py-6">
        <button
          className={"px-2 py-2 " + linkColor + " " + buttonCursor}
          name="home"
          onClick={onClick}
        >
          Now
        </button>
        <button className="text-white px-2 py-2" name="clock" onClick={onClick}>
          {timerLabel}
        </button>
        <div className="hidden lg:block px-2 py-2">{day}</div>
        <button
          className={"px-2 py-2 " + linkColor + " " + buttonCursor}
          name="dayUp"
          onClick={onClick}
        >
          Next day
        </button>
        <button
          className={"px-2 py-2 " + linkColor + " " + buttonCursor}
          name="dayDown"
          onClick={onClick}
        >
          Prev day
        </button>
        <div className="hidden lg:block px-2 py-2">
          {hour + ":" + minute + " " + ampm}
        </div>
        <button
          className={"px-2 py-2 " + linkColor + " " + buttonCursor}
          name="hourUp"
          onClick={onClick}
        >
          Next hour
        </button>
        <button
          className={"px-2 py-2 " + linkColor + " " + buttonCursor}
          name="hourDown"
          onClick={onClick}
        >
          Prev hour
        </button>
        <button
          className={"px-2 py-2 " + linkColor + " " + buttonCursor}
          name="minuteUp"
          onClick={onClick}
        >
          Next minute
        </button>
        <button
          className={"px-2 py-2 " + linkColor + " " + buttonCursor}
          name="minuteDown"
          onClick={onClick}
        >
          Prev minute
        </button>
      </div>
    </div>
  );
};

const WorldTimePanel = ({ locations, time }) => {
  const columns = _.chunk(locations, Math.ceil(locations.length / 2));

  return (
    <div className="lg:flex justify-center w-full py-4">
      <WorldTimeColumn column="0" locations={columns[0]} time={time} />
      <WorldTimeColumn column="1" locations={columns[1]} time={time} />
    </div>
  );
};

const WorldTimeColumn = ({ column, locations, time }) => {
  const locationsCount = locations.length;
  locations.map(function(location, i) {
    location.time = time.tz(location.timezone).format("ddd h:mm A");
    location.border = i === locationsCount - 1 ? "border-b-0" : "border-b";
    return location;
  });

  return (
    <div className={"w-full lg:w-1/2"}>
      {locations.map((loc, i) => (
        <div
          key={"c" + { column } + "-r" + i}
          className={"flex py-4 lg:p-0 border-grey lg:" + loc.border}
        >
          <div className="w-0 lg:w-1/5 flex items-center lg:px-8 lg:py-2">
            <img
              className="hidden lg:block"
              src={require("./images/" + loc.image + ".png")}
              alt="flag"
            />
          </div>
          <div className="w-1/2 lg:w-2/5 flex items-center text-2xl font-bold text-grey-darkest lg:pl-8">
            {loc.city}
          </div>
          <div className="w-1/2 lg:w-2/5 flex items-center text-2xl font-bold text-grey-dark">
            {loc.time}
          </div>
        </div>
      ))}
    </div>
  );
};

export default App;
