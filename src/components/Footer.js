import React, { Component } from "react";

class Footer extends Component {
  render() {
    return (
      <div>
        <div className="hidden lg:flex justify-center py-6 bg-grey">
          <div className="m-0 p-0">
            Icons made by &nbsp;
            <a
              href="https://www.flaticon.com/authors/freepik"
              title="Freepik"
              className="text-white"
            >
              Freepik
            </a>
            &nbsp; from &nbsp;
            <a
              href="https://www.flaticon.com/"
              title="Flaticon"
              className="text-white"
            >
              www.flaticon.com
            </a>
            &nbsp; and licensed by &nbsp;
            <a
              href="http://creativecommons.org/licenses/by/3.0/"
              title="Creative Commons BY 3.0"
              target="_blank"
              rel="noopener noreferrer"
              className="text-white"
            >
              CC 3.0 BY
            </a>
          </div>
        </div>

        <footer className="flex justify-center w-full bg-grey-darkest py-10">
          <a href="https://bob-humphrey.com">
            <img
              className="w-16"
              src={require("../images/bh-logo-grey.gif")}
              alt="Bob Humphrey website "
            />
          </a>
        </footer>
      </div>
    );
  }
}

export default Footer;
