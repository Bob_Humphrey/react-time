import React, { Component } from "react";

class Header extends Component {
  render() {
    const time = this.props.time;
    const day = time.format("dddd MMMM D");
    const hour = time.format("h");
    const minute = this.props.timer ? time.format("mm:ss") : time.format("mm");
    const ampm = time.format("A");

    return (
      <header className="lg:flex justify-between px-10 lg:px-20 p-8 lg:p-4 bg-grey-darkest">
        <div className="lg:w-1/3 lg:flex justify-start ">
          <h1 className="font-normal text-3xl text-white mb-2">
            {this.props.title}
          </h1>
        </div>

        <div className="lg:w-1/3 lg:flex justify-end text-2xl lg:text-3xl text-white">
          {day + " " + hour + ":" + minute + " " + ampm}
        </div>
      </header>
    );
  }
}

export default Header;
